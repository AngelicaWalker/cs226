/** Compare performance of SwapArraySet and MoveToFrontListSet.
    @author Angelica Walker
    awalke57
    awalke57@jhu.edu
    600.226.02
    04/10/17
    Assignment 7
*/

import com.github.phf.jb.Bench;
import com.github.phf.jb.Bee;

import java.util.Random;

/**
 * Compare performance of SwapArraySet and MoveToListSet.
 *
 * Sadly the code here is a tad bit messy since there's no elegant way to
 * instantiate and initialize a number of different set implementations.
 * Your benchmarking code for two classes will also be a bit messy. :-/
 */
public final class AdaptiveSetsBench {
    private static final int SIZE = 200; // you may need to tweak this...
    private static final Random RAND = new Random();

    private AdaptiveSetsBench() {}

    // First some basic "compound operations" to benchmark. Note that each
    // of these is carefully dimensioned (regarding the range of elements)
    // to allow combining them.

    // Insert a number of "consecutive" strings into the given set.
    private static void insertLinear(Set<String> s) {
        for (int i = 0; i < SIZE; i++) {
            s.insert(Integer.toString(i));
        }
    }

    // Insert a number of "random" strings into the given set.
    private static void insertRandom(Set<String> s) {
        for (int i = 0; i < SIZE; i++) {
            s.insert(Integer.toString(RAND.nextInt(SIZE * 4)));
        }
    }

    private static void insertHalfRand(Set<String> s) {
        for (int i = 0; i < SIZE; i++) {
            s.insert(Integer.toString(RAND.nextInt(SIZE / 2)));
        }
    }

    private static void insertSomeRand(Set<String> s) {
        for (int i = 0; i < SIZE; i++) {
            s.insert(Integer.toString(RAND.nextInt(SIZE / 50)));
        }
    }

    // Remove a number of "random" strings from the given set.
    private static void removeRandom(Set<String> s) {
        for (int i = 0; i < SIZE; i++) {
            s.remove(Integer.toString(RAND.nextInt(SIZE * 2)));
        }
    }

    // Lookup a number of "consecutive" strings in the given set.
    private static void lookupLinear(Set<String> s) {
        for (int i = 0; i < SIZE; i++) {
            boolean x = s.has(Integer.toString(i));
        }
    }

    // Lookup a number of "random" strings in the given set.
    private static void lookupRandom(Set<String> s) {
        for (int i = 0; i < SIZE; i++) {
            boolean x = s.has(Integer.toString(RAND.nextInt(SIZE)));
        }
    }

    // Now the benchmarks we actually want to run.

    @Bench
    public static void insertLinearSwapArraySet(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            Set<String> s = new SwapArraySet<>();
            b.start();
            insertLinear(s);
        }
    }

    @Bench
    public static void insertLinearMoveToFrontListSet(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            Set<String> s = new MoveToFrontListSet<>();
            b.start();
            insertLinear(s);
        }
    }

    @Bench
    public static void insertRandomSwapArraySet(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            Set<String> s = new SwapArraySet<>();
            b.start();
            insertRandom(s);
        }
    }

    @Bench
    public static void insertRandomMoveToFrontListSet(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            Set<String> s = new MoveToFrontListSet<>();
            b.start();
            insertRandom(s);
        }
    }

    @Bench
    public static void insertHalfRandomSwapArraySet(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            Set<String> s = new SwapArraySet<>();
            b.start();
            insertHalfRand(s);
        }
    }

    @Bench
    public static void insertHalfRandomMoveToFrontListSet(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            Set<String> s = new MoveToFrontListSet<>();
            b.start();
            insertHalfRand(s);
        }
    }

    @Bench
    public static void insertSomeRandomSwapArraySet(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            Set<String> s = new SwapArraySet<>();
            b.start();
            insertSomeRand(s);
        }
    }

    @Bench
    public static void insertSomeRandomMoveToFrontListSet(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            Set<String> s = new MoveToFrontListSet<>();
            b.start();
            insertSomeRand(s);
        }
    }
    
    @Bench
    public static void removeRandomSwapArraySet(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            Set<String> s = new SwapArraySet<>();
            insertRandom(s);
            b.start();
            removeRandom(s);
        }
    }

    @Bench
    public static void removeRandomMoveToFrontListSet(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            Set<String> s = new MoveToFrontListSet<>();
            insertRandom(s);
            b.start();
            removeRandom(s);
        }
    }

    @Bench
    public static void lookupLinearSwapArraySet(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            Set<String> s = new SwapArraySet<>();
            insertLinear(s);
            b.start();
            lookupLinear(s);
        }
    }

    @Bench
    public static void lookupLinearMoveToFrontListSet(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            Set<String> s = new MoveToFrontListSet<>();
            insertLinear(s);
            b.start();
            lookupLinear(s);
        }
    }

    @Bench
    public static void lookupRandomSwapArraySet(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            Set<String> s = new SwapArraySet<>();
            insertLinear(s);
            b.start();
            lookupRandom(s);
        }
    }

    @Bench
    public static void lookupRandomMoveToFrontListSet(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            Set<String> s = new MoveToFrontListSet<>();
            insertLinear(s);
            b.start();
            lookupRandom(s);
        }
    }
}
