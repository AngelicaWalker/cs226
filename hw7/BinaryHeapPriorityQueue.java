/** Binary heap priority queue.
    @author Angelica Walker
    awalke57
    awalke57@jhu.edu
    600.226.02
    04/10/17
    Assignment 7
*/

import java.util.ArrayList;
import java.util.Comparator;

/**
 * Priority queue implemented as a sorted ArrayList.
 *
 * We use binary search to find the insertion point in O(log n) time, but
 * we need to spend O(n) time to "make room" for the insertion. We don't
 * treat the array as cyclic, so remove() also takes O(n) time...
 *
 * We use slot 0 of the ArrayList as a "sentinel" of sorts for internal
 * find() calls.
 *
 * @param <T> Element type.
 */
public class BinaryHeapPriorityQueue<T extends Comparable<? super T>>
    implements PriorityQueue<T> {

    // The default comparator uses the "natural" ordering.
    private static class DefaultComparator<T extends Comparable<? super T>>
        implements Comparator<T> {
        public int compare(T t1, T t2) {
            return t1.compareTo(t2);
        }
    }

    private ArrayList<T> data;
    private Comparator<T> cmp;

    /**
     * A sorted array using the "natural" ordering of T.
     */
    public BinaryHeapPriorityQueue() {
        this(new DefaultComparator<>());
    }

    /**
     * A sorted array using the given comparator for T.
     * @param cmp Comparator to use.
     */
    public BinaryHeapPriorityQueue(Comparator<T> cmp) {
        this.data = new ArrayList<>();
        this.data.add(null);
        this.cmp = cmp;
    }

    // Value in slot i "less" than value in slot j? Note that the
    // comparator determines what we consider "less" here.
    private boolean less(int i, int j) {
        return this.cmp.compare(this.data.get(i), this.data.get(j)) < 0;
    }

    private void swap(int i, int j) {
        T tempOne = this.data.get(i);
        T tempTwo = this.data.get(j);
        this.data.set(i, tempTwo);
        this.data.set(j, tempOne);
    }

    private boolean hasLeft(int i) {
        return this.data.size() > (2 * i);
    }

    private boolean hasRight(int i) {
        return this.data.size() > ((2 * i) + 1);
    }

    private int leftIndex(int i) {
        return (2 * i);
    }

    private int rightIndex(int i) {
        return (2 * i) + 1;
    }

    private void leftSwap(int i) {
        if (this.less(2 * i, i)) {
            this.swap(2 * i, i);
        }
    }

    private void rightSwap(int i) {
        if (this.less((2 * i) + 1, i)) {
            this.swap((2 * i) + 1, i);
        }
    }

    private void removeSwap() {
        int current = 1;
        int temp = 0;

        while (this.hasLeft(current)) {
            if (!this.hasRight(current)) {
                temp = this.leftIndex(current);
                this.leftSwap(current);
                current = temp;
            }
	    else {
		if (this.less(this.rightIndex(current), this.leftIndex(current))) {
		    temp = this.rightIndex(current);
                    this.rightSwap(current);
                    current = temp;
                }
		else {
                    temp = this.leftIndex(current);
                    this.leftSwap(current);
                    current = temp;
                }
            }
        }
    }

    // Find position in data[] where t should live.
    private int find(T t) {
        this.data.set(0, t); // set sentinel
        int l = 1;
        int u = this.data.size() - 1;
        while (l <= u) {
            int m = (l + u) / 2;
            if (this.less(0, m)) {
                u = m - 1;
            } else if (this.less(m, 0)) {
                l = m + 1;
            } else {
                return m;
            }
        }
        return l;
    }

    @Override
    public void insert(T t) {
        int temp = this.data.size();
        this.data.add(temp, t);
        int result = temp / 2;
        while ((temp > 1) && (this.less(temp, result))) {
            this.swap(temp, result);
            temp = result;
            result = temp / 2;
        }
    }

    @Override
    public T remove() throws EmptyException {
        if (this.empty()) {
            throw new EmptyException();
        }
        return this.data.remove(1);
    }

    @Override
    public T best() throws EmptyException {
        if (this.empty()) {
            throw new EmptyException();
        }
        return this.data.get(1);
    }

    @Override
    public boolean empty() {
        return this.data.size() == 1;
    }

    @Override
    public String toString() {
        String temp = "[";
        for (int i = 1; i < this.data.size(); i++) {
            temp += this.data.get(i) + ",";
        }
        temp += "]";
        return temp;
    }
}
