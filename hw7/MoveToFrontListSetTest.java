/** JUnit testing for MoveToFrontListSet.
    @author Angelica Walker
    awalke57
    awalke57@jhu.edu
    600.226.02
    04/10/17
    Assignment 7
*/

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;
import java.util.Iterator;

/**
 * JUnit testing for MoveToFrontListSet.
 *
 */
public class MoveToFrontListSetTest {

    private Set<Integer> list;

    @Before
    public void setupListSet() {
        list = new MoveToFrontListSet<Integer>();
    }

    @Test
    public void testInsertNew() {
        list.insert(1);
	list.insert(5);
        
        String temp = "";
        
        for (int i : list) {
            temp += i + ",";
        }
        
        assertEquals("5,1,", temp);
        assertTrue(list.has(1));
        assertTrue(list.has(5));
    }
    
    @Test
    public void testInsertExisting() {
        list.insert(1);
        list.insert(5);
        list.insert(1);
        
        String temp = "";
        
        for (int i : list) {
            temp += i + ",";
        }
        
        assertEquals("1,5,", temp);
        assertTrue(list.has(5));
        assertTrue(list.has(1));
    }

    @Test
    public void testInsertRemoveInsert() {
        list.insert(1);
        list.remove(1);
        list.insert(1);

        String temp = "";

        for (int i : list) {
            temp += i + ",";
        }

        assertEquals("1,", temp);
        assertTrue(list.has(1));
    }
    
    @Test
    public void testRemove() {
        list.insert(1);
        list.insert(5);
        list.insert(10);
        list.remove(1);
        list.remove(10);
        
        String temp = "";
        
        for (int i : list) {
            temp += i + ",";
        }
        
        assertEquals("5,", temp);
        assertFalse(list.has(1));
        assertFalse(list.has(10));
    }
    
    @Test
    public void testRemoveEmpty() {
      
        String temp = "";
        
        assertEquals("", temp);
      
    }
    
    @Test
    public void testRemoveDNE() {
        list.insert(1);
        list.insert(5);
        list.remove(10);
        
        String temp = "";
        
        for (int i : list) {
            temp += i + ",";
        }
        
        assertEquals("5,1,", temp);
        assertFalse(list.has(10));
    }
    
    @Test
    public void testRemoveSameElementTwice() {
        list.insert(1);
        list.insert(2);
        list.insert(3);
        list.remove(3);
        list.remove(3);
        
        String temp = "";
        
        for (int i : list) {
        	temp += i + ",";
        }
        
        assertEquals("2,1,", temp);
        assertFalse(list.has(3));
    }
    
    @Test
    public void testHasInsert() {
        list.insert(1);
        assertTrue(list.has(1));
    }
    
    @Test
    public void testHasRemove() {
        list.insert(1);
        list.insert(5);
        list.insert(10);
        list.remove(5);
        
        assertFalse(list.has(5));
    }
    
    @Test
    public void iteratorWorks() {
        list.insert(1);
        list.insert(5);
        list.insert(10);
	
        Iterator<Integer> iterate = list.iterator();

        String temp = "";
        
        while(iterate.hasNext()) {
            temp += iterate.next() + ",";
        }
        
        assertEquals("10,5,1,", temp);
    }

}
