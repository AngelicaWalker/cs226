/** JUnit testing for SwapArraySet.
    @author Angelica Walker
    awalke57
    awalke57@jhu.edu
    600.226.02
    04/10/17
    Assignment 7
*/

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;
import java.util.Iterator;

/**
 * JUnit testing for SwapArraySet.
 *
 */
public class SwapArraySetTest {

    private Set<Integer> ra;

    @Before
    public void setupArraySet() {
	ra = new SwapArraySet<Integer>();
    }

    @Test
    public void testInsertNew() {
        ra.insert(1);
        ra.insert(5);
        
        String temp = "";
        
        for (int i : ra) {
            temp += i + ",";
        }
        
        assertEquals("1,5,", temp);
        assertTrue(ra.has(1));
        assertTrue(ra.has(5));
    }
    
    @Test
    public void testInsertExisting() {
        ra.insert(1);
        ra.insert(5);
        ra.insert(1);
        
        String temp = "";
        
        for (int i : ra) {
            temp += i + ",";
        }
        
        assertEquals("5,1,", temp);
        assertTrue(ra.has(1));
        assertTrue(ra.has(5));
    }

    @Test
    public void testInsertRemoveInsert() {
        ra.insert(1);
	ra.remove(1);
	ra.insert(1);

        String temp = "";

        for (int i : ra) {
            temp += i + ",";
        }

        assertEquals("1,", temp);
        assertTrue(ra.has(1));
    }
    
    @Test
    public void testRemove() {
        ra.insert(1);
        ra.insert(5);
	ra.insert(10);
	ra.remove(1);
	ra.remove(10);
        
        String temp = "";
        
        for (int i : ra) {
            temp += i + ",";
        }
        
        assertEquals("5,", temp);
        assertFalse(ra.has(5));
    }
    
    @Test
    public void testRemoveEmpty() {
               
        String temp = "";
                     
        assertEquals("", temp);

    }
    
    @Test
    public void testRemoveDNE() {
        ra.insert(1);
        ra.insert(5);
        ra.remove(10);
        
        String temp = "";
        
        for (int i : ra) {
            temp += i + ",";
        }

        assertEquals("1,5,", temp);
        assertFalse(ra.has(10));
    }

    @Test
    public void testRemoveSameElementTwice() {
        ra.insert(1);
        ra.insert(2);
        ra.insert(3);
        ra.remove(3);
        ra.remove(3);

        String temp = "";

        for (int i : ra) {
                temp += i + ",";
        }

        assertEquals("1,2,", temp);
        assertFalse(ra.has(3));
    }

    @Test
    public void testHasInsert() {
        ra.insert(1);
        assertTrue(ra.has(1));
    }

    @Test
    public void testHasRemove() {
        ra.insert(1);
        ra.insert(5);
        ra.insert(10);
        ra.remove(5);

        assertFalse(ra.has(5));
    }
    
    @Test
    public void iteratorWorks() {
        ra.insert(1);
        ra.insert(2);
        ra.insert(3);
        Iterator<Integer> iterate = ra.iterator();

        String temp = "";
        
        while(iterate.hasNext()) {
            temp += iterate.next() + ",";
        }
        
        assertEquals("1,2,3,", temp);
    }
    

}
