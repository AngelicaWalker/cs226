/** Filter unique integers from standard input to standard output.
    @author Angelica Walker
    awalke57
    awalke57@jhu.edu
    600.226.02
    04/10/17
    Assignment 7
*/

import java.util.Scanner;

/**
 * Filter unique integers from standard input to standard output.
 *
 * This version finally looks right: Using the appropriate data
 * structure we only have to write code that actually deals with
 * the problem at hand.
 *
 * If you're benchmarking this program, you may want to suppress
 * the output by redirecting it to /dev/null. Also note that the
 * Scanner class is horribly inefficient, alas it's the simplest
 * choice here.
 */
public final class Unique {
    private static Set<Integer> data;

    // Make checkstyle happy.
    private Unique() {}

    /**
     *  Main method.
     *  @param args Command line arguments (ignored).
     */
    public static void main(String[] args) {
        data = new ArraySet<Integer>();
        Scanner scanner = new Scanner(System.in);

        while (scanner.hasNext()) {
            String s = scanner.next();
            try {
                int i = Integer.parseInt(s);
                data.insert(i);
            } catch (NumberFormatException e) {
                System.err.printf("Ignored non-integer %s\n", s);
            }
        }

        for (Integer i: data) {
            System.out.println(i);
        }
    }
}
