/** Unique Queue implementation.
    @author Angelica Walker
    awalke57
    awalke57@jhu.edu
    600.226.02
    04/10/17
    Assignment 7
*/

import java.util.Scanner;

/**
 * Unique Queue implementation.
 *
 * This version finally looks right: Using the appropriate data
 * structure we only have to write code that actually deals with
 * the problem at hand.
 *
 * If you're benchmarking this program, you may want to suppress
 * the output by redirecting it to /dev/null. Also note that the
 * Scanner class is horribly inefficient, alas it's the simplest
 * choice here.
 */
public final class UniqueQueue {
    private static PriorityQueue<Integer> data;

    // Make checkstyle happy.
    private UniqueQueue() {}

    /**
     *  Main method.
     *  @param args Command line arguments (ignored).
     */
    public static void main(String[] args) {
	data = new BinaryHeapPriorityQueue<Integer>();
	Scanner scanner = new Scanner(System.in);

	while (scanner.hasNext()) {
	    String s = scanner.next();
	    try {
		int i = Integer.parseInt(s);
		data.insert(i);
	    } catch (NumberFormatException e) {
		System.err.printf("Ignored non-integer %s\n", s);
	    }
	}

	int head = data.best();
        int temp = data.best();
        System.out.println(head);
        data.remove();

	while (!data.empty()) {
            head = data.best();

	    if (head != temp) {
                System.out.println(head);
                temp = data.best();

	    }
            data.remove();
        }

    }
}
