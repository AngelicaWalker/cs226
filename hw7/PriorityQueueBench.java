/** Compare performance of SortedArrayPriorityQueue and BinaryHeapPriorityQueue.
    @author Angelica Walker
    awalke57
    awalke57@jhu.edu
    600.226.02
    04/10/17
    Assignment 7
*/

import com.github.phf.jb.Bench;
import com.github.phf.jb.Bee;

import java.util.Random;

/**
 * Compare performance of SortedArrayPriorityQueue and BinaryHeapPriorityQueue.
 *
 * Sadly the code here is a tad bit messy since there's no elegant way to
 * instantiate and initialize a number of different set implementations.
 * Your benchmarking code for two classes will also be a bit messy. :-/
 */
public final class PriorityQueueBench {
    private static final int SIZE = 200; // you may need to tweak this...
    private static final Random RAND = new Random();

    private PriorityQueueBench() {}

    // First some basic "compound operations" to benchmark. Note that each
    // of these is carefully dimensioned (regarding the range of elements)
    // to allow combining them.

    // Insert a number of "consecutive" strings into the given set.
    private static void insertLinear(PriorityQueue<String> s) {
        for (int i = 0; i < SIZE; i++) {
            s.insert(Integer.toString(i));
        }
    }

    // Insert a number of "random" strings into the given set.
    private static void insertRandom(PriorityQueue<String> s) {
        for (int i = 0; i < SIZE; i++) {
            s.insert(Integer.toString(RAND.nextInt(SIZE * 4)));
        }
    }

    private static void insertHalfRand(PriorityQueue<String> s) {
        for (int i = 0; i < SIZE; i++) {
            s.insert(Integer.toString(RAND.nextInt(SIZE / 2)));
        }
    }

    private static void insertSomeRand(PriorityQueue<String> s) {
        for (int i = 0; i < SIZE; i++) {
            s.insert(Integer.toString(RAND.nextInt(SIZE / 50)));
        }
    }

    // Remove a number of "random" strings from the given set.
    private static void remove(PriorityQueue<String> s) {
        for (int i = 0; i < SIZE; i++) {
            s.remove();
        }
    }

    // Now the benchmarks we actually want to run.

    @Bench
    public static void insertLinearSortedArrayPriorityQueue(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            PriorityQueue<String> s = new SortedArrayPriorityQueue<>();
            b.start();
            insertLinear(s);
        }
    }

    @Bench
    public static void insertLinearBinaryHeapPriorityQueue(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            PriorityQueue<String> s = new BinaryHeapPriorityQueue<>();
            b.start();
            insertLinear(s);
        }
    }

    @Bench
    public static void insertRandomSortedArrayPriorityQueue(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            PriorityQueue<String> s = new SortedArrayPriorityQueue<>();
            b.start();
            insertRandom(s);
        }
    }

    @Bench
    public static void insertRandomBinaryHeapPriorityQueue(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            PriorityQueue<String> s = new BinaryHeapPriorityQueue<>();
            b.start();
            insertRandom(s);
        }
    }
    
    @Bench
    public static void insertHalfRandomSortedArrayPriorityQueue(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            PriorityQueue<String> s = new SortedArrayPriorityQueue<>();
            b.start();
            insertHalfRand(s);
        }
    }

    @Bench
    public static void insertHalfRandomBinaryHeapPriorityQueue(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            PriorityQueue<String> s = new BinaryHeapPriorityQueue<>();
            b.start();
            insertHalfRand(s);
        }
    }

    @Bench
    public static void insertSomeRandomSortedArrayPriorityQueue(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            PriorityQueue<String> s = new SortedArrayPriorityQueue<>();
            b.start();
            insertSomeRand(s);
        }
    }

    @Bench
    public static void insertSomeRandomBinaryHeapPriorityQueue(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            PriorityQueue<String> s = new BinaryHeapPriorityQueue<>();
            b.start();
            insertSomeRand(s);
        }
    }

    @Bench
    public static void removeSortedArrayPriorityQueue(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            PriorityQueue<String> s = new SortedArrayPriorityQueue<>();
            insertRandom(s);
            b.start();
            remove(s);
        }
    }

    @Bench
    public static void removeBinaryHeapPriorityQueue(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            PriorityQueue<String> s = new BinaryHeapPriorityQueue<>();
            insertRandom(s);
            b.start();
            remove(s);
        }
    }
}
