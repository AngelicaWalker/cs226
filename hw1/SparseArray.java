/** Program to produce a Sparse array by implementing array.
 * @author Angelica Walker
 * awalke57
 * awalke57@jhu.edu
 * 600.226.02
 * 02/17/17
 * Assignment 2 Part 3
 */

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Program to produce a Sparse Array by implementing Array.
 * @param <T> element type.
 */
public class SparseArray<T> implements Array<T> {

    private static final class Node<T> {
        T data;
        Node<T> next;
        int position;

        Node(T t, Node<T> n, int pos) {
            this.data = t;
            this.next = n;
            this.position = pos;
        }

        int getNode() {
            return this.position;
        }

        void putNode(int posi) {
            this.position = posi;
        }
    }

    private Node<T> first;
    private int length;
    private Node<T> last;
    private T initial;
    private Node<T> hold;
    private int placeholder;

    /**
     * Method to produce the Sparse Array itself.
     *
     * @param n the length of the array.
     * @param t the initial value to store in each slot.
     * @throws LengthException if the length is negative.
     */
    public SparseArray(int n, T t) throws LengthException {
        this.length = n;
        this.last = new Node<T>(t, null, n);
        this.first = new Node<T>(t, this.last, n);
        this.initial = t;
        this.hold = new Node<T>(t, this.last, n);
        this.placeholder = 0;

        if (n <= 0) {
            throw new LengthException();
        }
    }

    private void prepend(int pos, T t) {
        Node<T> n = new Node<>(t, this.first, pos);
        this.first = n;
        this.length += 1;
    }

    private Node<T> find(int index) {
        Node<T> n = this.first;
        int p;
        for (int i = 0; i < this.length(); i++) {
            p = n.getNode();
            if (p == index) {
                return n;
            }
        }
        return null;
    }

    @Override
    public T get(int i) throws IndexException {
        Node<T> n = this.find(i);
        if (n == null) {
            return null;
        } else {
            return n.data;
        }
    }

    @Override
    public void put(int i, T t) throws IndexException {
        Node<T> n = this.find(i);
        if (n == null) {
            this.prepend(i, t);
        } else {
            n.data = t;
        }
    }

    @Override
    public int length() {
        return this.length;
    }

    @Override
    public Iterator<T> iterator() {
        return new SparseArrayIterator();
    }

    private final class SparseArrayIterator implements Iterator<T> {

        Node<T> current;

        SparseArrayIterator() {
            this.current = SparseArray.this.first;
        }

        @Override
        public T next() throws NoSuchElementException {
            T t = this.current.next.data;
            if (!this.hasNext()) {
                throw new NoSuchElementException();
            }
            this.current = this.current.next;
            return t;
        }

        @Override
        public boolean hasNext() {
            return this.current.next != SparseArray.this.last;
        }
    }

    @Override
    public String toString() {
        StringBuilder s = new StringBuilder();
        s.append("[");
        for (Node<T> n = this.first; n != null; n = n.next) {
            s.append(n.data);
            if (n.next != null) {
                s.append(", ");
            }
        }
        s.append("]");
        return s.toString();
    }
}
