/** Exception for invalid index.
 * @author Angelica Walker
 * awalke57
 * awalke57@jhu.edu
 * 600.226.02
 * 02/17/17
 * Assignment 2 Part 3 
 */

/**
 * Exception for invalid index.
 *
 * Data structures using (integer) indices throw IndexException
 * if a given index is out of range.
 */
public class IndexException extends RuntimeException {
    private static final long serialVersionUID = 0L;
}
