/** Simple polymorphic test framework for arrays.
 * @author Angelica Walker
 * awalke57
 * awalke57@jhu.edu
 * 600.226.02
 * 02/17/17
 * Assignment 2 Part 3
 */

import java.util.ArrayList; // see note in main() below

/**
 * Simple polymorphic test framework for arrays.
 *
 * See last week's PolyCount. You need to add more test cases (meaning more
 * methods like testNewLength and testNewWrongLength below) to make sure all
 * preconditions and axioms are indeed as expected from the specification.
 */
public final class PolyArray {
    private static final int LENGTH = 113;
    private static final int INITIAL = 7;

    private PolyArray() {}

    // methods for testing axioms go here

    private static void testNewLength(Array<Integer> a) {
        assert a.length() == LENGTH;
    }

    private static void testNewGet(Array<Integer> a) {
        for (int i = 0; i < LENGTH; i++) {
            assert a.get(i) == INITIAL;
        }
    }

    private static void testNewPut(Array<Integer> a) {
        for (int i = 0; i < LENGTH; i++) {
            a.put(i, i);
        }
        assert a.length() == LENGTH;
    }

    // methods for testing preconditions go here

    private static void testNewWrongLength() {
        try {
            Array<Integer> a = new SimpleArray<>(0, INITIAL);
            assert false;
        } catch (LengthException e) {
            // passed the test, nothing to do
        }
        try {
            Array<Integer> b = new ListArray<>(0, INITIAL);
            assert false;
        } catch (LengthException e) {
            // passed the test, nothing to do
        }
        try {
            Array<Integer> c = new SparseArray<>(0, INITIAL);
            assert false;
        } catch (LengthException e) {
            // passed the test, nothing to do
        }
    }

    private static void testGetWrongIndex() {
        Array<Integer> a = new SimpleArray<>(0, INITIAL);
        Array<Integer> b = new ListArray<>(0, INITIAL);
        Array<Integer> c = new SparseArray<>(0, INITIAL);
        try {
            a.get(a.length());
            assert false;
        } catch (IndexException e) {
            // passed the test, do nothing
        }
        try {
            b.get(b.length());
            assert false;
        } catch (IndexException e) {
            // passed the test, do nothing
        }
        try {
            c.get(c.length());
            assert false;
        } catch (IndexException e) {
            // passed the test, do nothing
        }
    }

    private static void testPutWrongIndex() {
        Array<Integer> a = new SimpleArray<>(0, INITIAL);
        Array<Integer> b = new ListArray<>(0, INITIAL);
        Array<Integer> c = new SparseArray<>(0, INITIAL);
        try {
            a.put(3000, 200);
            assert false;
        } catch (IndexException e) {
            // passed the test, do nothing
        }
        try {
            b.put(3000, 200);
            assert false;
        } catch (IndexException e) {
            // passed the test, do nothing
        }
        try {
            c.put(3000, 200);
            assert false;
        } catch (IndexException e) {
            // passed the test, do nothing
        }
    }

    /**
     * Run (mostly polymorphic) tests on various array implementations.
     *
     * Make sure you run this with -enableassertions! We'll learn a much
     * better approach to unit testing later.
     *
     * @param args Command line arguments (ignored).
     */
    public static void main(String[] args) {
        // For various technical reasons, we cannot use a plain Java array
        // here like we did in PolyCount. Sorry.
        ArrayList<Array<Integer>> arrays = new ArrayList<>();
        arrays.add(new SimpleArray<Integer>(LENGTH, INITIAL));
        arrays.add(new ListArray<Integer>(LENGTH, INITIAL));
        arrays.add(new SparseArray<Integer>(LENGTH, INITIAL));

        // Test all the axioms. We can do that nicely in a loop. In the test
        // methods, keep in mind that you are handed the same object over and
        // over again!
        for (Array<Integer> a: arrays) {
            testNewLength(a);
            testNewGet(a);
            testNewPut(a);
        }

        // Test all the preconditions. Sadly we have to code each one of these
        // out manually, not even Java's reflection API would help...
        testNewWrongLength();
        testGetWrongIndex();
        testPutWrongIndex();
    }
}
