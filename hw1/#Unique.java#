/** Program to print each unique integer in a command line.
 * @author Angelica Walker
 * awalke57
 * awalke57@jhu.edu
 * 600.226.02
 * 02/17/17
 * Assignment 2 Part 2
 */

import java.util.Scanner;
import java.io.IOException;

/**
 * This is a sample-solution for Problem 1 on Assignment 1. It doesn't use
 * anything fancy, just stuff you should have known from your previous Java
 * courses. (Well, and some stuff checkstyle would have told you about.)
*/
public final class Unique {
    // array of unique numbers
    private static Array<Integer> data;
    // how many slots in data are used?
    private static int used;

    // make checkstyle happy
    private Unique() {}

    // position of given value in data[], -1 if not found
    private static int find(int value) {
        for (int i = 0; i < used; i++) {
            if (data.get(i) == value) {
                return i;
            }
        }
        return -1;
    }

    // insert value into data[] if not already present
    private static void insert(int value) {
        int position = find(value);
        if (position < 0) {

            if (used == data.length()) {
                data = doubleSize(data);
            }

            data.put(used, new Integer(value));
            used += 1;
        }
    }

    /**
     * Method used to double the size of the array when it is filled.
     *
     * @param inputData the array passed into the method.
     * @return newData the new array with double the size.
     */
    public static Array<Integer> doubleSize(Array<Integer> inputData) {
        Array<Integer> newData =
            new SimpleArray<Integer>(inputData.length() * 2, null);

        for (int i = 0; i < inputData.length(); i++) {
            newData.put(i, inputData.get(i));
        }

        return newData;

    }


    /**
     * Print only unique integers out of given command line arguments.
     *
     * @param args Command line arguments.
     * @throws IOException if there is an error.
     */
    public static void main(String[] args) throws IOException {

        Scanner keys = new Scanner(System.in);

        // worst case: args.length distinct numbers
        data = new SimpleArray<Integer>(5, null);

        // process args and insert unique numbers into data[]
        while (keys.hasNext()) {
            String arg;
            arg = keys.next();

            try {
                int i = Integer.parseInt(arg);
                insert(i);
            } catch (NumberFormatException e) {
                System.err.printf("Ignored non-integer argument %s\n", arg);
            }
        }
        // output unique numbers in array order
        for (int i = 0; i < used; i++) {
            System.out.println(data.get(i));
        }
    }
}
