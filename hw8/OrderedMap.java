/** Ordered maps from comparable keys to arbitrary values
    @author Angelica Walker
    awalke57
    awalke57@jhu.edu
    600.226.02
    04/19/17
    Assignment 8
*/

/**
 * Ordered maps from comparable keys to arbitrary values.
 *
 * @param <K> Type for keys.
 * @param <V> Type for values.
 */
public interface OrderedMap<K extends Comparable<? super K>, V>
    extends Map<K, V> {
}
