/** Benchmark for BinarySearchTree
    @author Angelica Walker
    awalke57
    awalke57@jhu.edu
    600.226.02
    04/19/17
    Assignment 8
*/

import com.github.phf.jb.Bench;
import com.github.phf.jb.Bee;

import java.util.Random;

/**
 * Benchmark for BinarySearchTree.
 *
 */
public final class BinarySearchTreeBench {

    private static final int SIZE = 200;
    private static final Random RAND = new Random();
    private static final String[] TESTSTR = {"test1", "test2", "test3"};

    private BinarySearchTreeBench() {}

    private static void insertLinear(Map<Integer, String> m) {
        for (int i = 0; i < SIZE; i++) {
            m.insert(i, TESTSTR[RAND.nextInt(TESTSTR.length - 1)]);
        }
    }

    private static void insertRandom(Map<Integer, String> m) {
        for (int i = 0; i < SIZE; i++) {
            int temp = RAND.nextInt(SIZE * 2);

            if (!m.has(temp)) {
                m.insert(temp, TESTSTR[RAND.nextInt(TESTSTR.length - 1)]);
            }

        }
    }

    private static void removeRandom(Map<Integer, String> m) {
        for (int i = 0; i < SIZE; i++) {
            int temp = RAND.nextInt(SIZE * 2);

            if (m.has(temp)) {
                m.remove(temp);
            }

        }
    }

    private static void lookupLinear(Map<Integer, String> m) {
        for (int i = 0; i < SIZE; i++) {
            boolean check = m.has(i);
        }
    }

    private static void lookupRandom(Map<Integer, String> m) {
        for (int i = 0; i < SIZE; i++) {
            boolean check = m.has(RAND.nextInt(SIZE));
        }
    }

    private static void putLinear(Map<Integer, String> m) {
        for (int i = 0; i < SIZE; i++) {
            m.put(i, TESTSTR[RAND.nextInt(TESTSTR.length - 1)]);
        }
    }

    private static void putRandom(Map<Integer, String> m) {
        for (int i = 0; i < SIZE; i++) {
            int temp = RAND.nextInt(SIZE * 2);
            if (m.has(temp)) {
                m.put(temp, TESTSTR[RAND.nextInt(TESTSTR.length - 1)]);
            }
        }
    }

    private static void getLinear(Map<Integer, String> m) {
        for (int i = 0; i < SIZE; i++) {
            String s = m.get(i);
        }
    }

    private static void getRandom(Map<Integer, String> m) {
        for (int i = 0; i < SIZE; i++) {
            int temp = RAND.nextInt(SIZE);

            if (m.has(temp)) {
                String s = m.get(RAND.nextInt(SIZE));
            }

        }
    }

    // Now the benchmarks we actually want to run.
    /** insert test.
     *
     * @param b bee.
     */
    @Bench
    public static void insertLinearBinarySearchTreeMap(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            Map<Integer, String> m = new BinarySearchTreeMap<>();
            b.start();
            insertLinear(m);
        }
    }

    /** insert random.
     *
     * @param b bee.
     */
    @Bench
    public static void insertRandomBinarySearchTreeMap(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            Map<Integer, String> m = new BinarySearchTreeMap<>();
            b.start();
            insertRandom(m);
        }
    }

    /** remove random.
     *
     * @param b bee.
     */
    @Bench
    public static void removeRandomBinarySearchTreeMap(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            Map<Integer, String> m = new BinarySearchTreeMap<>();
            insertRandom(m);
            b.start();
            removeRandom(m);

        }
    }

    /** put random.
     *
     * @param b bee.
     */
    @Bench
    public static void putRandomBinarySearchTreeMap(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            Map<Integer, String> m = new BinarySearchTreeMap<>();
            insertRandom(m);
            b.start();
            putRandom(m);

        }
    }

    /** put linear.
     *
     * @param b bee.
     */
    @Bench
    public static void putLinearBinarySearchTreeMap(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            Map<Integer, String> m = new BinarySearchTreeMap<>();
            insertLinear(m);
            b.start();
            putLinear(m);

        }
    }

    /** look up linear.
     *
     * @param b bee.
     */
    @Bench
    public static void lookupLinearBinarySearchTreeMap(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            Map<Integer, String> m = new BinarySearchTreeMap<>();
            insertLinear(m);
            b.start();
            lookupLinear(m);
        }
    }

    /** look up random.
     *
     * @param b bee.
     */
    @Bench
    public static void lookupRandomBinarySearchTreeMap(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            Map<Integer, String> m = new BinarySearchTreeMap<>();
            insertLinear(m);
            b.start();
            lookupRandom(m);

        }
    }

    /** get linear.
     *
     * @param b bee.
     */
    @Bench
    public static void getLinearBinarySearchTreeMap(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            Map<Integer, String> m = new BinarySearchTreeMap<>();
            insertLinear(m);
            b.start();
            getLinear(m);

        }
    }

    /** get random.
     *
     * @param b bee.
     */
    @Bench
    public static void getRandomBinarySearchTreeMap(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            Map<Integer, String> m = new BinarySearchTreeMap<>();
            insertLinear(m);
            b.start();
            getRandom(m);

        }
    }




}
