import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

/**
 * Treap Map implementation.
 *
 * @param <K> Key type.
 * @param <V> Value type.
 */
public class TreapMap<K extends Comparable<? super K>, V>
    implements OrderedMap<K, V> {

    private class Node {
        Node left;
        Node right;
        K key;
        V value;
        Random rand = new Random();
        int priority;

        Node(K k, V v) {
            this.key = k;
            this.value = v;
            this.priority = this.rand.nextInt();
        }
    }


    private Node root;
    private int size;
    private StringBuilder stringBuilder;

    @Override
    public int size() {
        if (this.root == null) {
            return 0;
        } else {
            return this.size;
        }
    }

    // Return node for given key. This one is iterative but a recursive
    // one would also work. It's just that there's no real advantage to
    // using recursion for this operation.
    private Node find(K k) {
        if (k == null) {
            throw new IllegalArgumentException("cannot handle null key");
        }
        Node n = this.root;
        while (n != null) {
            int cmp = k.compareTo(n.key);
            if (cmp < 0) {
                n = n.left;
            } else if (cmp > 0) {
                n = n.right;
            } else {
                return n;
            }
        }
        return null;
    }

    private Node findNode(K k) {
        Node n = this.find(k);
        if (n == null) {
            throw new IllegalArgumentException("cannot find key " + k);
        }
        return n;
    }

    private Node leftRotation(Node n) {
        Node a = n.right;
        n.right = a.left;
        a.left = n;
        return a;
    }

    private Node rightRotation(Node n) {
        Node a = n.left;
        n.left = a.right;
        a.right = n;
        return a;
    }

    @Override
    public boolean has(K k) {
        if (k == null) {
            return false;
        }
        return this.find(k) != null;
    }

    @Override
    public void put(K k, V v) throws IllegalArgumentException {
        Node n = this.findNode(k);
        n.value = v;
    }

    @Override
    public V get(K k) throws IllegalArgumentException {
        Node a = this.findNode(k);
        return a.value;
    }

    // Insert given key and value into subtree rooted at given node;
    // return changed subtree with new node added. Unlike in find()
    // above, doing this recursively *has* benefits: First we get
    // away with simpler code that doesn't need parent pointers,
    // second the recursive structure makes it easier to add fancy
    // rebalancing code later.
    private Node insert(Node n, K k, V v) {
        if (n == null) {
            return new Node(k, v);
        }

        int cmp = k.compareTo(n.key);
        if (cmp < 0) {

            n.left = this.insert(n.left, k, v);

            if (n.left.priority < n.priority) {
                n = this.rightRotation(n);

            }
        } else if (cmp > 0) {

            n.right = this.insert(n.right, k, v);

            if (n.right.priority < n.priority) {
                n = this.leftRotation(n);

            }
        } else {
            throw new IllegalArgumentException("duplicate key " + k);
        }

        return n;
    }

    @Override
    public void insert(K k, V v) throws IllegalArgumentException {
        if (k == null) {
            throw new IllegalArgumentException("cannot handle null key");
        }

        this.root = this.insert(this.root, k, v);
        this.size += 1;
    }

    // Return node with maximum key in subtree rooted at given node.
    // (Iterative because once again recursion has no advantage.)
    private Node max(Node n) {
        while (n.right != null) {
            n = n.right;
        }
        return n;
    }


    // Remove given node and return the remaining tree. Easy if the node
    // has 0 or 1 child; if it has two children, find the predecessor,
    // copy its data to the given node (thus removing the key we need to
    // get rid off), the remove the predecessor node.
    private Node remove(Node n) {

        if (n.left == null) {
            return n.right;
        }
        if (n.right == null) {
            return n.left;
        }

        Node max = this.max(n.left);
        n.key = max.key;
        n.value = max.value;
        n.left = this.remove(n.left, max.key);
        return n;
    }

    // Remove node with given key from subtree rooted at given node;
    // return changed subtree with given key missing. (Again doing this
    // recursively makes it easier to add fancy rebalancing code later.)
    private Node remove(Node n, K k) {
        if (n == null) {
            throw new IllegalArgumentException("cannot find key " + k);
        }

        int cmp = k.compareTo(n.key);
        if (cmp < 0) {
            n.left = this.remove(n.left, k);
        } else if (cmp > 0) {
            n.right = this.remove(n.right, k);
        } else {
            n = this.remove(n);
        }

        return n;
    }

    @Override
    public V remove(K k) throws IllegalArgumentException {
        Node n = this.find(k);
        this.root = this.remove(this.root, k);
        this.size -= 1;
        return n.value;
    }

    // Recursively add keys from subtree rooted at given node into the
    // given list in order.
    private void iteratorHelper(Node n, List<K> keys) {
        if (n == null) {
            return;
        }
        this.iteratorHelper(n.left, keys);
        keys.add(n.key);
        this.iteratorHelper(n.right, keys);
    }

    @Override
    public Iterator<K> iterator() {
        List<K> keys = new ArrayList<K>();
        this.iteratorHelper(this.root, keys);
        return keys.iterator();
    }

    // If we don't have a StringBuilder yet, make one;
    // otherwise just reset it back to a clean slate.
    private void setupStringBuilder() {
        if (this.stringBuilder == null) {
            this.stringBuilder = new StringBuilder();
        } else {
            this.stringBuilder.setLength(0);
        }
    }

    // Recursively append string representations of keys and values from
    // subtree rooted at given node in order.
    private void toStringHelper(Node n, StringBuilder s) {
        if (n == null) {
            return;
        }
        this.toStringHelper(n.left, s);
        s.append(n.key);
        s.append(": ");
        s.append(n.value);
        s.append(", ");
        this.toStringHelper(n.right, s);
    }

    @Override
    public String toString() {
        this.setupStringBuilder();
        this.stringBuilder.append("{");

        this.toStringHelper(this.root, this.stringBuilder);

        int length = this.stringBuilder.length();
        if (length > 1) {
            // If anything was appended at all, get rid of
            // the last ", " the toStringHelper put in.
            this.stringBuilder.setLength(length - 2);
        }
        this.stringBuilder.append("}");

        return this.stringBuilder.toString();
    }
}
