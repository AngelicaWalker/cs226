/** Instantiate the LinkedList to test.
    @author Angelica Walker
    awalke57
    awalke57@jhu.edu
    600.226.02
    03/08/17
    Assignment 5
 */

/** Instantiate the LinkedList to test. */
public class LinkedListTest extends ListTestBase {
    @Override
    protected List<String> createList() {
        return new LinkedList<>();
    }
}
