/** Testing implementations of the List interface.
    @author Angelica Walker
    awalke57
    awalke57@jhu.edu
    600.226.02
    03/82/17
    Assignment 5
 */

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

/**
 * Testing implementations of the List interface.
 *
 * The tests defined here apply to all implementations of the List
 * interface. However, they cannot be run directly as we don't know
 * which implementation to test or how to create an instance of it.
 *
 * The solution is to define a "template method" called createList()
 * that subclasses of this test override. The LinkedListTest.java class,
 * for example, creates a suitable LinkedList instance to be tested.
 *
 * (We could use a JUnit feature called "parameterized tests" to do
 * the same thing, however that feature is a bit more complex to use
 * than we would like.)
 *
 * Note that we (somewhat arbitrarily) choose to test lists of strings.
 * We could have gone for lists of integers or lists of whatever, but
 * strings seem convenient in any case: You can pick strings in such a
 * way as to make your test cases more readable.
 */
public abstract class ListTestBase {
    private List<String> list;

    protected abstract List<String> createList();

    @Before
    public void setupListTests() {
        list = this.createList();
    }

    @Test
    public void newListEmpty() {
        assertTrue(list.empty());
        assertEquals(0, list.length());
        assertEquals("[]", list.toString());

        int c = 0;
        for (String s: list) {
            c++;
        }
        assertEquals(0, c);
    }

    @Test(expected=EmptyException.class)
    public void newListNoFront() {
        Position<String> p = list.front();
    }

    @Test(expected=EmptyException.class)
    public void newListNoBack() {
        Position<String> p = list.back();
    }

    @Test
    public void insertFrontWorks() {
        list.insertFront("One");
        list.insertFront("Two");
        list.insertFront("Three");

        assertFalse(list.empty());
        assertEquals(3, list.length());
        assertEquals("[Three, Two, One]", list.toString());

        int c = 0;
        for (String s: list) {
            c++;
        }
        assertEquals(3, c);
    }

    @Test
    public void insertBackWorks() {
        list.insertBack("One");
        list.insertBack("Two");
        list.insertBack("Three");

        assertFalse(list.empty());
        assertEquals(3, list.length());
        assertEquals("[One, Two, Three]", list.toString());

        int c = 0;
        for (String s: list) {
            c++;
        }
        assertEquals(3, c);
    }

    @Test
    public void insertFrontBackConsistent() {
        Position<String> f = list.insertFront("Front");
        assertEquals("Front", f.get());
        Position<String> b = list.insertBack("Back");
        assertEquals("Back", b.get());

        assertNotEquals(f, b);

        assertTrue(list.first(f));
        assertTrue(list.last(b));

        Position<String> x;

        x = list.front();
        assertEquals(f, x);

        x = list.back();
        assertEquals(b, x);
    }

    @Test
    public void removeFrontWorks() {
        list.insertFront("One");
        list.insertFront("Two");
        list.insertFront("Three");
        list.removeFront();
        list.removeFront();

        assertFalse(list.empty());
        assertEquals(1, list.length());
        assertEquals("[One]", list.toString());

        int c = 0;
        for (String s: list) {
            c++;
        }
        assertEquals(1, c);
    }

    @Test
    public void removeBackWorks() {
        list.insertFront("One");
        list.insertFront("Two");
        list.insertFront("Three");
        list.removeBack();
        list.removeBack();

        assertFalse(list.empty());
        assertEquals(1, list.length());
        assertEquals("[Three]", list.toString());

        int c = 0;
        for (String s: list) {
            c++;
        }
        assertEquals(1, c);
    }

    // TODO You need to add *many* more test cases here, ideally before you
    // even start working on SentinelList!

    @Test
    public void testGet() {

        Position<String> t = list.insertFront("5");
        assertEquals(t.get(), "5");
    }

    @Test
    public void testPut() {

	Position<String> t = list.insertFront("5");
        t.put("10");
        assertEquals(p.get(), "10");
    }

    @Test
    public int testLength() {

	assertEquals(0, list.length());

	Position<String> t = list.insertFront("5");
    	list.removeBack();

	assertEquals(1, list.length());
    }

    @Test
    public void testInsertBefore() {

	Position<String> t = list.insertBack("3");;
    	list.insertBefore(t, "2345");
    	assertEquals(list.previous(t).get(), "2345");
    }

    @Test
    public void testInsertAfter() {

        Position<String> t = list.insertFront("3");;
        list.insertAfter(t, "2345");
        assertEquals(list.previous(t).get(), "2345");

    }


    @Test
    public void testAllInserts() {

        Position<String> a = list.insertFront("4");
    	Position<String> b = list.insertFront("1");
    	Position<String> c = list.insertAfter(b, "3");
    	Position<String> d = list.insertBefore(c, "2");
    	Position<String> e = list.insertBack("6");
    	Position<String> f = list.insertBefore(e,"5");
    	assertEquals("[1, 2, 3, 4, 5, 6]", list.toString());
    	

    }

    @Test(expected = PositionException.class)
    public void testInsertBeforeException() {

        Position<String> a = list.insertBack("5");
        Position<String> b = list.previous(list.next(a));
        list.insertBefore(b, "10");
	
    }

    @Test(expected = PositionException.class)
    public void	testInsertAfterException() {

      	Position<String> a = list.insertFront("5");
        Position<String> b = list.next(list.next(a));
        list.insertAfter(b, "10");

    }

    @Test(expected = PositionException.class)
    public void testInsertBeforeHead(){

	Position<String> a = list.insertBack("5");
    	Position<String> b = list.previous(a);
    	list.insertBefore(b, "10");

    }

    @Test(expected = PositionException.class)
    public void testInsertAfterTail(){

	Position<String> a = list.insertBack("5");
    	Position<String> b = list.next(a);
    	list.insertAfter(b, "10");

    }

    @Test(expected = PositionException.class)
    public void testInsertAfterHead(){

    	Position<String> a = list.insertBack("5");
    	Position<String> b = list.previous(a);
    	list.insertAfter(b, "10");
    	assertEquals(list.front().get(), "10");

    }

    @Test(expected = PositionException.class)
    public void testInsertBeforeTail(){

	Position<String> a = list.insertBack("5");
    	Position<String> b = list.next(a);
    	list.insertBefore(b, "10");
    	assertEquals(list.back().get(), "10");

    }

    @Test
    public void testRemove() {

	Position<String> a = list.insertFront("testing");
    	Position<String> b = list.insertBack("563");
    	Position<String> c = list.insertBack("2345");
    	list.remove(b);
        assertEquals(list.next(a), c);

    }

    @Test(expected = PositionException.class)
    public void testRemoveInvalid() {

	Position<String> a = list.insertFront("5");
    	Position<String> b = list.insertBack("10");
       	list.remove(a);
    	list.insertBefore(list.previous(b), "10");

    }

    @Test(expected = PositionException.class)
    public void testRemoveException() {

	Position<String> a = list.insertFront("5");
    	Position<String> b = list.previous(a);
    	list.remove(b);

    }

    @Test(expected = EmptyException.class)
    public void testRemoveFrontException() {

	list.removeFront();

    }

    @Test(expected = EmptyException.class)
    public void testRemoveBackException() {

	list.removeBack();

    }

    @Test
    public void testFirst() {

	Position<String> a = list.insertFront("5");
    	Position<String> b = list.insertFront("10");
 
    	assertEquals(list.first(b), true);
    	assertEquals(list.first(a), false);

    }

    @Test
    public void testLast() {

	Position<String> a = list.insertFront("5");
	Position<String> b = list.insertFront("10");

        assertEquals(list.last(a), true);
	assertEquals(list.last(b), false);

    }

    @Test(expected = PositionException.class)
    public void testFirstException() {

    	Position<String> a = list.insertFront("5");
        list.first(list.previous(list.previous(a)));

    }

    @Test(expected = PositionException.class)
    public void testLastException() {

        Position<String> a = list.insertBack("5");
        list.first(list.next(list.next(a)));

    }

    @Test
    public void testNext() {

	Position<String> a = list.insertFront("5");
        Position<String> b = list.insertFront("10");
        assertEquals(list.next(b).get(), "5");

    }

    
    @Test
    public void testPrev() {

	Position<String> a = list.insertFront("5");
    	Position<String> b = list.insertFront("10");
    	assertEquals(list.previous(a).get(), "10");

    }

    @Test(expected = PositionException.class)
    public void testNextException() {

	Position<String> a = list.insertFront("5");
        Position<String> b = list.next(a);
        list.next(b);

    }


    @Test(expected = PositionException.class)
    public void testPrevException() {

	Position<String> a = list.insertFront("5");
        Position<String> b = list.previous(a);
	list.previous(b);

    }

    @Test
    public void testIteratorForward(){

	Position<String> a = list.insertFront("10");
    	Position<String> b = list.insertFront("5");
        String str = "";
        Iterator<String> test = list.forward();
	
        while (test.hasNext()) {
            str = str + test.next();
        }
	
        assertEquals("510", str);
	
    }

    @Test
    public void testIteratorBackwards() {

	Position<String> a = list.insertFront("5");
    	Position<String> b = list.insertFront("10");
        String str = "";
        Iterator<String> test = list.backward();

	while (test.hasNext()) {
            str = str + test.next();
        }

	assertEquals("510", str);
	
    }

    @Test
    public void testString() {

	Position<String> a = list.insertFront("10");
    	Position<String> b = list.insertFront("5");
        String str = list.toString();

	assertEquals("[5, 10]", str);

    }

    @Test
    public void testStringEmpty() {

	String str = list.toString();
        assertEquals("[]", str);
	
    }

    @Test
    public void testStringRemove() {

	Position<String> a = list.insertFront("10");
    	Position<String> b = list.insertFront("removed");
    	Position<String> c = list.insertFront("5");
    	list.remove(b);
        String str = list.toString();

	assertEquals("[5, 10]", str);

    }

    
}
