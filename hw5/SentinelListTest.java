/** Test program for the Sentinel List.
    @author Angelica Walker
    awalke57
    awalke57@jhu.edu
    600.226.02
    03/08/17
    Assignment 5
 */
public class SentinelListTest extends ListTestBase {
    @Override
    protected List<String> createList() {
        return new SentinelList<>();
    }
}
