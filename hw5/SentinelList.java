/** List with Sentinels for efficiency.
    @author Angelica Walker
    awalke57
    awalke57@jhu.edu
    600.226.02
    03/08/17
    Assignment 5
 */

import java.util.Iterator;

/** List with Senitnels for efficiency.

    @param <T> Element Type.
 */
public class SentinelList<T> implements List<T> {

    private static final class Node<T> implements Position<T> {

        Node<T> next;
        Node<T> prev;
        T data;

        List<T> owner;

        @Override
        public T get() {
            return this.data;
        }

        @Override
        public void put(T t) {
            this.data = t;
        }
    }


    private final class SentinelIterator implements Iterator<T> {

        Node<T> current;
        Node<T> last;
        boolean forward;

        SentinelIterator(boolean f) {

            this.forward = f;
            if (this.forward) {
                this.current = SentinelList.this.head.next;
                this.last = SentinelList.this.tail;
            } else {
                this.current = SentinelList.this.tail.prev;
                this.last = SentinelList.this.head;
            }

        }

        @Override
        public T next() {

            T t = this.current.get();

            if (this.forward) {
                this.current = this.current.next;
            } else {
                this.current = this.current.prev;
            }

            return t;
        }

        @Override
        public boolean hasNext() {

            return this.current != null;

        }

        @Override
        public void remove() {

            throw new UnsupportedOperationException();

        }

    }


    private Node<T> head;
    private Node<T> tail;
    private int length;

    /** Defining a SentinelList.
     */
    public SentinelList() {

        this.head = new Node<T>();
        this.tail = new Node<T>();
        this.head.next = this.tail;
        this.tail.prev = this.head;

    }

    @Override
    public Iterator<T> iterator() {

        return this.forward();

    }


    private Node<T> convert(Position<T> p) throws PositionException {
        try {
            Node<T> n = (Node<T>) p;
            if (n.owner != this) {
                throw new PositionException();
            }
            return n;
        } catch (NullPointerException | ClassCastException e) {
            throw new PositionException();
        }
    }

    @Override
    public boolean empty() {

        return this.length == 0;

    }

    @Override
    public int length() {

        return this.length;

    }


    @Override
    public Position<T> insertFront(T t) {

        Node<T> n = new Node<T>();

        n.owner = this;
        n.data = t;
        n.prev = this.head;
        n.next = this.head.next;

        this.head.next.prev = n;
        this.head.next = n;
        this.length++;

        return n;
    }

    @Override
    public Position<T> insertBack(T t) {

        Node<T> n = new Node<T>();

        n.owner = this;
        n.data = t;
        n.prev = this.tail.prev;
        n.next = this.tail;

        this.tail.prev.next = n;
        this.tail.prev = n;
        this.length++;

        return n;
    }

    @Override
    public Position<T> insertBefore(Position<T> p, T t)
        throws PositionException {

        Node<T> n = new Node<T>();
        Node<T> temp = this.convert(p);

        n.owner = this;
        n.data = t;
        n.prev = temp.prev;
        n.next = temp;

        temp.prev.next = n;
        temp.prev = n;
        this.length++;

        return n;

    }

    @Override
    public Position<T> insertAfter(Position<T> p, T t)
        throws PositionException {

        Node<T> n = new Node<T>();
        Node<T> temp = this.convert(p);

        n.owner = this;
        n.data = t;
        n.prev = temp;
        n.next = temp.next;

        temp.next.prev = n;
        temp.next = n;
        this.length++;

        return n;

    }

    @Override
    public void remove(Position<T> p) throws PositionException {

        Node<T> temp = this.convert(p);

        temp.owner = null;
        temp.prev.next = temp.next;
        temp.next.prev = temp.prev;
        this.length--;

    }

    @Override
    public void removeFront() throws EmptyException {

        if (this.empty()) {
            throw new EmptyException();
        }

        this.head.next.owner = null;
        Node<T> nextOne = this.head.next.next;
        nextOne.prev = this.head;
        this.head.next = nextOne;
        this.length--;

    }

    @Override
    public void removeBack() throws EmptyException {

        if (this.empty()) {
            throw new EmptyException();
        }

        this.tail.prev.owner = null;
        Node<T> previousOne = this.tail.prev.prev;
        previousOne.next = this.tail;
        this.tail.prev = previousOne;
        this.length--;

    }

    @Override
    public Position<T> front() throws EmptyException {

        if (this.empty()) {
            throw new EmptyException();
        }

        return this.head.next;
    }

    @Override
    public Position<T> back() throws EmptyException {

        if (this.empty()) {
            throw new EmptyException();
        }

        return this.tail.prev;

    }

    @Override
    public boolean first(Position<T> p) throws PositionException {

        return this.head.next == this.convert(p);

    }

    @Override
    public boolean last(Position<T> p) throws PositionException {

        return this.tail.prev == this.convert(p);

    }

    @Override
    public Position<T> next(Position<T> p) throws PositionException {

        return this.convert(this.convert(p).next);

    }

    @Override
    public Position<T> previous(Position<T> p) throws PositionException {

        return this.convert(this.convert(p).prev);

    }

    @Override
    public Iterator<T> forward() {

        return new SentinelIterator(true);

    }

    @Override
    public Iterator<T> backward() {

        return new SentinelIterator(false);

    }

    /** Method to turn array into a string.

        @return str the array as a string.
     */
    public String toString() {

        String str = "[";

        for (Node<T> p = this.head.next; p != this.tail; p = p.next) {
            str = str + p.data;
            if (p.next != this.tail) {
                str = str + ", ";

            }
        }

        str = str + "]";

        return str;
    }

}
