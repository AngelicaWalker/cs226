/** Test program for the Sparse Graph.
    @author Angelica Walker
    awalke57
    awalke57@jhu.edu
    600.226.02
    03/29/17
    Assignment 6 pt 1
*/
public class SparseGraphTest extends GraphTestBase {

    @Override
    protected Graph<String, String> createGraph() {
        return new SparseGraph<>();
    }
    
}
