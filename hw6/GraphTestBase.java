/** Testing implementations for the Graph interface.
    @author Angelica Walker
    awalke57
    awalke57@jhu.edu
    600.226.02
    03/29/17
    Assignment 6 pt 1
*/

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

/**
 * Testing implementations of the List interface.
 */
public abstract class GraphTestBase {

    private Graph<String, String> graph;
    private Graph<String, String> graph2;
    private Vertex<String> vertex;
    private Edge<String> edge;

    protected abstract Graph<String, String> createGraph();

    @Before
    public void setupGraphTests() {
        graph = this.createGraph();
	graph = this.createGraph();
    }

    @Test
    public void getPutWorksVertex() {

	vertex.put("test");
	assertEquals("test", vertex.get());

    }

    @Test
    public void getPutWorksEdge() {

	edge.put("test");
	assertEquals("test", edge.get());

    }

    @Test
    public void vertexInsert() {

	boolean test = false;
	vertex = graph.insert("New vertex");

	for (Vertex<String> vertexTest : graph.vertices()) {

	    if (vertexTest == vertex) {

		test = true;

	    }
	}

	assertTrue(test);
    }

    @Test
    public void edgeInsert() {

	boolean test = false;

	vertex = graph.insert("Starting vertex");
	Vertex<String> vertex2 = graph.insert("Ending vertex");
        edge = graph.insert(vertex, vertex2, "newEdge");

	for (Edge<String> edgeTest : graph.edges()) {

	    if (edgeTest == edge) {

		test = true;

	    }

	}

	assertTrue(test);
    }
    
    @Test(expected=PositionException.class)
    public void edgeInsertPosException() {

	vertex = graph.insert("same vertex");
	graph.insert(vertex, vertex, "newEdge");
	
    }
    
    @Test(expected=InsertionException.class)
    public void edgeInsertInsertException() {

	vertex = graph.insert("vertex");
	Vertex<String> vertex2 = graph.insert("vertex2");
	graph.insert(vertex, vertex2, "Edge");
	graph.insert(vertex, vertex2, "Edge");
	
    }
    
    @Test
    public void vertexRemove() {

	boolean test = false;
	
	vertex = graph.insert("vertex");
        assertEquals("vertex", graph.remove(vertex));

        for (Vertex<String> vertexTest : graph.vertices()) {

	    if (vertexTest == vertex) {

		test = true;

	    }
        }

	assertFalse(test);
	
    }

    @Test(expected=PositionException.class)
    public void vertexRemovePosException() {

	graph.remove(vertex);
    }

    @Test(expected=RemovalException.class)
    public void vertexRemoveRemException() {

	vertex = graph.insert("This string is in graph");
	Vertex<String> vertex2 = graph2.insert("This string is in graph2");
	graph.remove(vertex2);
	
    }

    @Test
    public void edgeRemove() {

	boolean test = false;
	
	vertex = graph.insert("vertex");
	Vertex<String> vertex2 = graph.insert("vertex2");
	edge = graph.insert(vertex, vertex2, "edge");
	assertEquals("edge", graph.remove(edge));

        for (Edge<String> edgeTest : graph.edges()) {

	    if (edgeTest == edge) {

		test = true;

	    }
        }

	assertFalse(test);

    }

    @Test(expected=PositionException.class)
    public void edgeRemovePosException() {

        vertex = graph.insert("vertex");
        Vertex<String> vertex2 = graph.insert("vertex2");
	edge = graph.insert(vertex, vertex2, "edge");

	graph2.remove(edge);
	
    }

    @Test
    public void iterableEdgeOutgoing() {

	vertex = graph.insert("vertex");
	Vertex<String> vertex2 = graph.insert("vertex2");
	Vertex<String> vertex3 = graph.insert("vertex3");

	edge = graph.insert(vertex, vertex2, "edge");
	Edge<String> edge2 = graph.insert(vertex, vertex3, "edge2");

	int testing = 0;

	for (Edge<String> edgeTest : graph.outgoing(vertex)) {

	    if (edgeTest == edge) {

		testing = testing + 1;
	    }

	    if (edgeTest == edge2) {

		testing = testing + 1;

	    }
	}

	assertEquals(testing, 2);
    }

    @Test(expected=PositionException.class)
    public void iterableEdgeOutgoingPosException() {

	graph.outgoing(null);
	
    }

    @Test
    public void iterableEdgeIncoming() {

        vertex = graph.insert("vertex");
        Vertex<String> vertex2 = graph.insert("vertex2");
        Vertex<String> vertex3 = graph.insert("vertex3");

        edge = graph.insert(vertex, vertex2, "edge");
        Edge<String> edge2 = graph.insert(vertex3, vertex2, "edge2");

        int testing = 0;

        for (Edge<String> edgeTest : graph.outgoing(vertex2)) {

            if (edgeTest == edge) {

		testing	= testing + 1;
            }

            if (edgeTest == edge2) {

		testing	= testing + 1;

            }
	}

        assertEquals(testing, 2);

    }

    @Test(expected=PositionException.class)
    public void iterableEdgeIncomingPosException() {

	graph.incoming(null);
    }

    @Test
    public void toAndFrom() {

        vertex = graph.insert("vertex");
        Vertex<String> vertex2 = graph.insert("vertex2");
	edge = graph.insert(vertex, vertex2, "edge");

	assertEquals(vertex, graph.to(edge));
	assertEquals(vertex2, graph.from(edge));

    }

    @Test(expected=PositionException.class)
    public void fromPosException() {

        vertex = graph.insert("vertex");
        Vertex<String> vertex2 = graph.insert("vertex2");
	Vertex<String> vertex3 = graph2.insert("vertex3");
	Vertex<String> vertex4 = graph2.insert("vertex4");

	edge = graph.insert(vertex, vertex2, "edge");
	Edge<String> edge2 = graph2.insert(vertex3, vertex4, "edge2");

        graph.from(edge2);

    }

    @Test(expected=PositionException.class)
    public void toPosException() {

        vertex = graph.insert("vertex");
        Vertex<String> vertex2 = graph.insert("vertex2");
        Vertex<String> vertex3 = graph2.insert("vertex3");
        Vertex<String> vertex4 = graph2.insert("vertex4");

        edge = graph.insert(vertex, vertex2, "edge");
        Edge<String> edge2 = graph2.insert(vertex3, vertex4, "edge2");

        graph.to(edge2);

    }

    @Test
    public void labelVertex() {

	vertex = graph.insert("vertex");
	graph.label(vertex, "label");

	assertEquals(graph.label(vertex), "label");
    }

    @Test(expected=PositionException.class)
    public void labelVertexPosException(){

	vertex = graph.insert("vertex");

	graph2.label(vertex);
    }

    @Test(expected=PositionException.class)
    public void	labelVertexPosExceptionNullVertex(){

	vertex = null;

	graph.label(vertex);
	
    }
    
    @Test
    public void labelEdge() {
	
	vertex = graph.insert("vertex");
	Vertex<String> vertex2 = graph.insert("vertex2");
	edge = graph.insert(vertex, vertex2, "edge");

	graph.label(edge, "label");

	assertEquals(graph.label(edge), "label");
    }

    @Test(expected=PositionException.class)
    public void labelEdgePosException() {

        vertex = graph.insert("vertex");
	Vertex<String> vertex2 = graph.insert("vertex2");

	edge = graph.insert(vertex, vertex2, "Edge");

	graph2.label(edge);
    }

    @Test(expected=PositionException.class)
    public void labelEdgePosExceptionNullEdge(){

	edge = null;

	graph.label(edge);
    }

    @Test
    public void clearLabelsTest() {

        vertex = graph.insert("vertex");
        Vertex<String> vertex2 = graph.insert("vertex2");
        edge = graph.insert(vertex, vertex2, "edge");


	graph.label(vertex, "A");
	graph.label(vertex2, "B");
	graph.label(edge, "C");
	
	graph.clearLabels();

	assertEquals(graph.label(vertex), null);
	assertEquals(graph.label(vertex2), null);
	assertEquals(graph.label(edge), null);
    }

    @Test
    public void toStringTest() {

        vertex = graph.insert("A");
        Vertex<String> vertex2 = graph.insert("B");
        Edge<String> edge = graph.insert(vertex, vertex2, "Edge");

	String test = "digraph {\n \"A\";\n \"B\";\n \"A\" -> \"B\" [label=\"Edge\"];\n}";
	
        assertEquals(graph.toString(), test);
    }
    
}


    
