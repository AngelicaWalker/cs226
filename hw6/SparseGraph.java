/** "Efficient" implementation of Graph<V, E>.
    @author Angelica Walker
    awalke57
    awalke57@jhu.edu
    600.226.02
    03/29/17
    Assignment 6 pt 1
*/

import java.util.ArrayList;

/** "Efficient" implementation of Graph.
    @param <V> Vertex element type.
    @param <E> Edge element type.
*/
public class SparseGraph<V, E> implements Graph<V, E> {

    ArrayList<Vertex<V>> totalVertices;
    ArrayList<Edge<E>> totalEdges;

    SparseGraph() {

        this.totalVertices = new ArrayList<Vertex<V>>();
        this.totalEdges = new ArrayList<Edge<E>>();

    }

    private class Verty implements Vertex<V> {

        ArrayList<Edge<E>> outgoing;
        ArrayList<Edge<E>> incoming;
        Object label;
        V data;
	Graph<V, E> color;

        Verty(V v, Graph<V, E> newColor) {

            this.outgoing = new ArrayList<Edge<E>>();
            this.incoming = new ArrayList<Edge<E>>();
            this.data = v;
	    this.color = newColor;

        }

        @Override
        public V get() {

            return this.data;

        }

        @Override
        public void put(V v) {

            this.data = v;

        }

    }

    private class Edgy implements Edge<E> {

        Vertex<V> from;
        Vertex<V> to;
        Object label;
        E data;
	Graph<V, E> color;

        Edgy(Vertex<V> newFrom, Vertex<V> newTo, E e, Graph<V, E> newColor) {

            this.from = newFrom;
            this.to = newTo;
            this.data = e;
	    this.color = newColor;
        }

        @Override
        public E get() {

            return this.data;

        }

        @Override
        public void put(E e) {

            this.data = e;

        }
    }

    private Verty convert(Vertex<V> v) {

        SparseGraph<V, E>.Verty vertex;

        if (v == null) {

            throw new PositionException();

        }

        vertex = (SparseGraph<V, E>.Verty) v;

        if (vertex.color != this) {

            throw new PositionException();

        }

        return vertex;
    }


    private Edgy convert(Edge<E> e) {

        SparseGraph<V, E>.Edgy edge;

        if (e == null || !(e instanceof SparseGraph<?, ?>.Edgy)) {

            throw new PositionException();

        }

        edge = (Edgy) e;

        if (edge.color != this) {

            throw new PositionException();

        }

        return edge;
    }


    @Override
    public Vertex<V> insert(V v) {

        Vertex<V> newVertex = new Verty(v, this);
        this.totalVertices.add(newVertex);
        return newVertex;

    }

    @Override
    public Edge<E> insert(Vertex<V> from, Vertex<V> to, E e)
        throws PositionException, InsertionException {

        Verty vertyFrom = this.convert(from);
        Verty vertyTo = this.convert(to);

        if (from == to) {

            throw new InsertionException();

        }

        for (Edge<E> edge : this.convert(from).outgoing) {

            if (to == this.convert(edge).to) {

                throw new PositionException();

            }

        }

        Edge<E> newEdge = new Edgy(from, to, e, this);
        vertyFrom.outgoing.add(newEdge);
        vertyTo.incoming.add(newEdge);
        this.totalEdges.add(newEdge);

        return newEdge;

    }

    @Override
    public V remove(Vertex<V> v)
        throws PositionException, RemovalException {

        Verty vertex = this.convert(v);

        if (!vertex.outgoing.isEmpty() || !vertex.incoming.isEmpty()) {

            throw new PositionException();

        }

        if (!this.totalVertices.contains(v)) {

            throw new RemovalException();

        }

        this.totalVertices.remove(v);

        return v.get();
    }

    @Override
    public E remove(Edge<E> e) throws PositionException {

        if (!this.totalEdges.contains(e)) {

            throw new PositionException();

        }

        Edgy edge = this.convert(e);
        Verty temp = this.convert(edge.from);

        temp.outgoing.remove(e);
        temp = this.convert(edge.to);
        temp.incoming.remove(e);

        this.totalEdges.remove(e);

        return e.get();
    }

    @Override
    public Iterable<Vertex<V>> vertices() {

        ArrayList<Vertex<V>> vertex
            = new ArrayList<Vertex<V>>(this.totalVertices);

        return vertex;

    }

    @Override
    public Iterable<Edge<E>> edges() {

        ArrayList<Edge<E>> edge = new ArrayList<Edge<E>>(this.totalEdges);

        return edge;

    }

    @Override
    public Iterable<Edge<E>> outgoing(Vertex<V> v) throws PositionException {

        Verty vertex = this.convert(v);

        if (vertex.outgoing.isEmpty()) {

            throw new PositionException();

        }

        return vertex.outgoing;

    }

    @Override
    public Iterable<Edge<E>> incoming(Vertex<V> v) throws PositionException {

        Verty vertex = this.convert(v);

        if (vertex.incoming.isEmpty()) {

            throw new PositionException();

        }

        return vertex.incoming;
    }

    @Override
    public Vertex<V> from(Edge<E> e) throws PositionException {

        Edgy edge = this.convert(e);

        if (!this.totalEdges.contains(edge)) {

            throw new PositionException();

        }

        return edge.from;

    }

    @Override
    public Vertex<V> to(Edge<E> e) throws PositionException {

        Edgy edge = this.convert(e);

        if (!this.totalEdges.contains(edge)) {

            throw new PositionException();

        }

        return edge.to;
    }

    @Override
    public void label(Vertex<V> v, Object l) throws PositionException {

        Verty vertex = this.convert(v);

        if (!this.totalVertices.contains(vertex) || l == null) {

            throw new PositionException();

        }

        vertex.label = l;

    }

    @Override
    public void label(Edge<E> e, Object l) throws PositionException {

        Edgy edge = this.convert(e);

        if (!this.totalEdges.contains(e) || l == null) {

            throw new PositionException();

        }

        edge.label = l;

    }

    @Override
    public Object label(Vertex<V> v) throws PositionException {

        Verty vertex = this.convert(v);

        if (!this.totalVertices.contains(vertex)) {

            throw new PositionException();

        }

        return vertex.label;

    }

    @Override
    public Object label(Edge<E> e) throws PositionException {

        Edgy edge = this.convert(e);

        if (!this.totalEdges.contains(e)) {

            throw new PositionException();

        }

        return edge.label;

    }

    @Override
    public void clearLabels() {

        for (Edge<E> e : this.totalEdges) {

            Edgy edge = this.convert(e);

            edge.label = null;

        }

        for (Vertex<V> v : this.totalVertices) {

            Verty vertex = this.convert(v);

            vertex.label = null;

        }
    }

    /**Method to turn the graph into a string.
       @return printGraph the string of the graph.
     */
    public String toString() {

        String printGraph = "diagraph {\n";

        for (Vertex<V> v : this.totalVertices) {

            Verty vertex = this.convert(v);
            String label = (String) vertex.label;

            printGraph += " \"" + vertex.data + "\";\n";

        }

        for (Edge<E> e : this.totalEdges) {

            Edgy edge = this.convert(e);
            Verty vertexFrom = this.convert(edge.from);
            Verty vertexTo = this.convert(edge.to);

            printGraph += " \"" + vertexFrom.data + "\" -> \""
                + vertexTo.data + "\" ";
            printGraph += "[label=\"" + edge.data + "\"];\n";

        }

        printGraph += "}";

        return printGraph;

    }
}
