/** Vertex position for graph.
    @author Angelica Walker
    awalke57
    awalke57@jhu.edu
    600.226.02
    03/27/17
    Assignment 6 pt 1
*/

/**
 * Vertex position for graph.
 * @param <T> Element type.
 */
public interface Vertex<T> extends Position<T> {
}
