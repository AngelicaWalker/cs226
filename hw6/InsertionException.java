/** Exception for bad insertions.
    @author Angelica Walker
    awalke57
    awalke57@jhu.edu
    600.226.02
    03/29/17
    Assignment 6 pt 1
*/

/**
 * Exception for bad insertions.
 *
 * Some data structures don't like certain insertions. For example, the Tree
 * interface doesn't allow insertRoot() if there's already a root, the Graph
 * interface doesn't allow insertEdge() if the edge would create a self-loop,
 * etc.
 */
public class InsertionException extends RuntimeException {
    private static final long serialVersionUID = 0L;
}
