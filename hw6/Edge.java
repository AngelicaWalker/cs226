/** Edge position for graph.
    @author Angelica Walker
    awalke57
    awalke57@jhu.edu
    600.226.02
    03/29/17
    Assignment 6 pt 1
*/

/**
 * Edge position for graph.
 * @param <T> Element type.
 */
public interface Edge<T> extends Position<T> {
}
