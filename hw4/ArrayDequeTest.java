/** Program to test the ArrayDeque program.
    @author Angelica Walker
    awalke57
    awalke57@jhu.edu
    600.226.02
    03/02/17
    Assignment 4 Part 2
*/

import org.junit.BeforeClass;
import static org.junit.Assert.*;
import org.junit.Test;


public class ArrayDequeTest {

    
    static Deque<Integer> test;

    @BeforeClass
    public static void setUp() {
	
        test = new ArrayDeque<Integer>();
    }

    @Test
    public void emptyTest() {
	
        ArrayDeque<String> testing = new ArrayDeque<String>();
        assertEquals(true, testing.empty());
    }

    @Test
    public void lengthTest() {
	
        ArrayDeque<String> testing = new ArrayDeque<String>();
        testing.insertBack("5");
        testing.insertFront("10");
        testing.insertBack("35");
        testing.insertFront("4");
        assertEquals(4, testing.length());

    }
    
    @Test
    public void frontTest() {
	
        ArrayDeque<String> testing = new ArrayDeque<String>();
        testing.insertFront("10");
        assertEquals("10", testing.front());
    }
    
    @Test
    public void backTest() {
	
        ArrayDeque<String> testing = new ArrayDeque<String>();
        testing.insertBack("2");
        testing.insertBack("8");
        assertEquals("8", testing.back());
    }

    @Test
    public void insertFrontTest() {
	
        ArrayDeque<String> testing = new ArrayDeque<String>();
        testing.insertFront("3");
	testing.insertBack("60");
        assertEquals("3", testing.front());
    }

    @Test
    public void insertBackTest() {
	
        ArrayDeque<String> testing = new ArrayDeque<String>();
        testing.insertBack("72");
        testing.insertBack("0");
        testing.insertFront("600");
        assertEquals("0", testing.back());

    }

    @Test
    public void removeFrontTest() {
	
        ArrayDeque<String> testing = new ArrayDeque<String>();
        testing.insertFront("20974");
        testing.insertFront("900");
        testing.insertFront("0");
        testing.removeFront();
        assertEquals("900", testing.front());
    }

    @Test
    public void removeBackTest() {

        ArrayDeque<String> testing = new ArrayDeque<String>();
        testing.insertBack("10985");
	testing.insertFront("0");
        testing.insertBack("2192");
        testing.insertBack("00");
        testing.removeBack();
        assertEquals("2192", testing.back());
    }


    @Test
    public void toStringTest() {

	String testStr = "[23, 2902, 10]";
        ArrayDeque<String> testing = new ArrayDeque<String>();
        testing.insertBack("23");
        testing.insertBack("2902");
        testing.insertBack("10");
        assertEquals(testStr, testing.toString());
    }


    @Test(expected = EmptyException.class)
    public void testEmptyExcep() throws EmptyException {
        ArrayDeque<String> testing = new ArrayDeque<String>();
        test.removeFront();
    }

}
