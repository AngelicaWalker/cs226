/** Program that performs calculations.
    @author Angelica Walker
    awalke57
    awalke57@jhu.edu
    600.226.02
    03/02/17
    Assignment 4 Part 1
 */

import java.util.Scanner;

/** Program that performs calculations.

 */
public final class Calc {

    private void calc() {

    }
    /** Verify that the input is correct.

        @param i the string that is being verified.
        @return true if it is correct.
    */
    public static boolean verify(String i) {

        try {

            Integer.parseInt(i);
            return true;
        } catch (NumberFormatException e) {
            System.out.println("#Invalid Input.");
            return false;
        }

    }

    /** Main method.

        @param args not used.
    */
    public static void main(String[] args) {

        Stack<Integer> inputList = new ArrayStack<Integer>();
        Scanner keys = new Scanner(System.in);
        String input = "";
        boolean quit = false;

        while (!quit && keys.hasNext()) {

            input = keys.next();

            if (verify(input)) {
                inputList.push(Integer.parseInt(input));
            }

            if (("?").equals(input)) {
                System.out.println(inputList);
            }

            if (("^").equals(input)) {
                System.out.println(inputList.top());
                inputList.pop();
            }

            if (("!").equals(input)) {
                quit = true;
            }

            if (("+").equals(input) && (!inputList.empty())) {

                int j = inputList.top();

                try {
                    inputList.pop();
                    int k = inputList.top();
                    int add = j + k;
                    inputList.pop();
                    inputList.push(add);
                } catch (EmptyException e) {
                    System.out.println("#Not enough arguements.");
                    inputList.push(j);
                }

            }

            if (("-").equals(input) && (!inputList.empty())) {

                int j = inputList.top();

                try {
                    inputList.pop();
                    int k = inputList.top();
                    int sub = j - k;
                    inputList.pop();
                    inputList.push(sub);
                } catch (EmptyException e) {
                    System.out.println("#Not enough arguements.");
                    inputList.push(j);
                }

            }

            if (("*").equals(input) && (!inputList.empty())) {

                int j = inputList.top();

                try {
                    inputList.pop();
                    int k = inputList.top();
                    int mult = j * k;
                    inputList.pop();
                    inputList.push(mult);
                } catch (EmptyException e) {
                    System.out.println("#Not enough arguements.");
                    inputList.push(j);
                }
            }

            if (("/").equals(input) && (!inputList.empty())) {

                int j = inputList.top();

                try {
                    inputList.pop();
                    int k = inputList.top();
                    int div = j / k;
                    inputList.pop();
                    inputList.push(div);
                } catch (EmptyException e) {
                    System.out.println("#Not enough arguements.");
                    inputList.push(j);
                } catch (ArithmeticException e) {
                    System.out.println("#Not enough arguements.");
                    inputList.push(j);
                }
            }

            if (("%").equals(input) && (!inputList.empty())) {

                int j = inputList.top();

                try {
                    inputList.pop();
                    int k = inputList.top();
                    int mod = k % k;
                    inputList.pop();
                    inputList.push(mod);
                } catch (EmptyException e) {
                    System.out.println("#Not enough arguements.");
                    inputList.push(j);
                } catch (ArithmeticException e) {
                    System.out.println("#Not enough arguements.");
                    inputList.push(j);
                }

            }

        }
    }
}
