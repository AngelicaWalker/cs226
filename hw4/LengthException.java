/** Exception for invalid length..
    @author Angelica Walker
    awalke57
    awalke57@jhu.edu
    600.226.02
    03/02/17
    Assignment 4
 */

/**
 * Exception for invalid length.
 *
 * Data structures that have a fixed (integer) length throw
 * LengthException if a given length is out of range.
 */
public class LengthException extends RuntimeException {
    private static final long serialVersionUID = 0L;
}
