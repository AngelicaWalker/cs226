/** Program to create an Array Deque.
    @author Angelica Walker
    awalke57
    awalke57@jhu.edu
    600.226.02
    03/02/17
    Assignment 4 Part 2
*/

/** Program to create an Array Deque.

    @param <T> Element Type.
*/
public class ArrayDeque<T> implements Deque<T> {

    private T[] data;
    private int front;
    private int back;
    private int used;
    private boolean usedBack;
    private boolean usedFront;

    /** Functions for Array Deque.
     */
    public ArrayDeque() {
        this.data = (T[]) new Object[1];

    }


    @Override
    public boolean empty() {

        if (this.used == 0) {
            return true;
        }

        return false;
    }

    @Override
    public int length() {

        return this.used;
    }

    @Override
    public T front() throws EmptyException {

        return this.data[this.front];
    }

    @Override
    public T back() throws EmptyException {

        return this.data[this.back - 1];
    }

    @Override
    public void insertFront(T t) {

        if (this.full()) {

            this.growDouble(this.front);
        }

        this.data[(this.front - 1 + this.data.length) % this.data.length] = t;

        this.front = (this.front - 1 + this.data.length) % this.data.length;

        this.usedFront = true;
        this.used++;
    }

    @Override
    public void insertBack(T t) {
        if (this.full()) {
            this.growDouble(this.back);
        }

        this.data[this.back] = t;
        this.back++;
        this.usedBack = true;
        this.used++;
    }

    @Override
    public void removeFront() throws EmptyException {

        if (this.empty()) {
            throw new EmptyException();
        }

        this.front = (this.front + 1 + this.data.length) % this.data.length;

        this.used--;
    }

    @Override
    public void removeBack() throws EmptyException {

        if (this.empty()) {
            throw new EmptyException();
        }

        this.back = (this.back - 1 + this.data.length) % this.data.length;

        this.used--;
    }


    /** Method to determine if it is full.

        @return the new length.
     */
    public boolean full() {

        return this.length() == this.data.length;
    }

    private void growDouble(int x) {

        T[] doubling = (T[]) new Object[this.data.length * 2];
        int leng = doubling.length;

        if (this.usedFront) {

            for (int j = this.data.length - 1; j > this.front; j--) {

                leng--;
                doubling[leng] = this.data[j];
            }

        }

        for (int k = 0; k < this.back; k++) {
            doubling[k] = this.data[k];
        }

        this.data = doubling;

        if (x == this.front) {
            this.front = leng;
        }
    }


    /** Method to make array a string.

        @return the array as a string.
     */
    public String toString() {

        String str = "[";

        for (int i = 0; i < this.used; i++) {

            str = str +  this.data[(this.front + i + this.data.length)
                   % this.data.length].toString();

            if (i < this.used - 1) {
                str = str + ", ";
            }

        }
        str = str + "]";

        return str;
    }

}

