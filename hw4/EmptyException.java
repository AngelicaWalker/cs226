/** Exception for empty data structure..
    @author Angelica Walker
    awalke57
    awalke57@jhu.edu
    600.226.02
    03/02/17
    Assignment 4 Part 1
 */

/**
 * Exception for empty data structure.
 *
 * Data structures that can be empty throw EmptyException
 * if asked to produce a value when they have none.
 */
public class EmptyException extends RuntimeException {
    private static final long serialVersionUID = 0L;
}
