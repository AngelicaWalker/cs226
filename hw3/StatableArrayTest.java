/** Program to test Statable Array using JUnit 4
 * @author Angelica Walker
 * awalke57
 * awalke57@jhu.edu
 * 600.226.02
 * 02/25/17
 * Assignment 3 Part 1
 */


/*
    Testing the Statable Array implementation.

    This is a first example of using JUnit4 for testing. The JUnit framework
    expects us to @annotate our testing code so it can run it for us. Below
    we use only a few @annotations:

    @Test marks test cases. Each test case should focus on a single property
    we would like to verify, so ideally there is only one assertion in each of
    these methods. Writing many small test cases is better than writing a few
    big ones: the more fine-grained our tests, the easier it is to focus on
    exactly what the problem is should a test case fail. We try to give test
    cases long and descriptive names for the same reason.

    @Before marks per-test setup code. These methods are run once before each
    @Test method and ensure that we always start testing from the same state.
    This makes test cases more independent in that the order in which they are
    executed doesn't matter.

    @BeforeClass marks per-class setup code. These methods are run once before
    all @Test methods. They are reserved for expensive setup code and we will
    not have too much use for them. Since the same instances are retained for
    all test cases, the order in which we run tests matters again. However, if
    using @Before would take too long, we have to pay that price.

    We do not usually require that JUnit test cases be checkstyle-compliant.
    The main reason for this is that we want you to focus on writing the test
    cases, not on writing Javadoc for them. However, we will still take a look
    at your tests. If the code is simply too chaotic, then we will still take
    points off. (Your best bet would be to still run checkstyle on test cases
    but to ignore warnings regarding Javadoc.)
*/

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class StatableArrayTest {

    final String INITIAL = "initialized";
    final int LENGTH = 75;

    StatableArray<String> array;

    @Before
    public void setupArray() {
        array = new StatableArray<String>(LENGTH, INITIAL);
    }

    @Test
    public void newRead(){
        assertEquals(array.numberOfReads(), 0);
    }

    @Test
    public void testGet(){
        array.get(5);
	assertEquals(array.numberOfReads(), 1);
    }

    @Test
    public void testPut(){
	array.put(3, "test");
        assertEquals(array.numberOfWrites(), 1);
    }

    @Test
    public void testLength(){
	array.length();
        assertEquals(array.numberOfReads(), 1);
    }

    @Test
    public void newWrite(){
        assertEquals(array.numberOfWrites(), 0);
    }

    @Test
    public void testGetLength(){
	array.get(3);
	array.length();
        assertEquals(array.numberOfReads(), 2);
    }

    @Test
    public void testWrites(){
	array.get(3);
        assertEquals(array.numberOfWrites(), 0);
    }

    @Test
    public void testRead(){
	array.put(2, "true");
        array.put(3, "test");
        assertEquals(array.numberOfReads(), 0);
    }

    @Test
    public void testResetRead(){
	array.get(3);
	array.resetStatistics();
        assertEquals(array.numberOfReads(), 0);
    }

    @Test
    public void testResetWrite(){
        array.put(3, "test");
        array.resetStatistics();
	assertEquals(array.numberOfWrites(), 0);
    }

    @Test
    public void testResetNewRead(){
        array.resetStatistics();
	assertEquals(array.numberOfReads(), 0);
    }

    @Test
    public void testResetNewWrite(){
	array.resetStatistics();
	assertEquals(array.numberOfWrites(), 0);
    }
}
