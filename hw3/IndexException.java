/** Exception for invalid index.
    @author Angelica Walker
    awalke57
    awalke57@jhu.edu
    600.226.02
    02/25/17
    Assignment 3 Part 1
 */

/**
 * Exception for invalid index.
 *
 * Data structures using (integer) indices throw IndexException
 * if a given index is out of range.
 */
public class IndexException extends RuntimeException {
    private static final long serialVersionUID = 0L;
}
