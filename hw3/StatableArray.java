/** Counters that keep track of whether an array has been written or read.
    @author Angelica Walker
    awalke57
    awalke57@jhu.edu
    600.226.02
    02/25/17
    Assignment 3 Part 1
 */


/** Counters that keep track of whether an array has been written or read.

    @param <T> Element Type.
 */
public class StatableArray<T> extends SimpleArray<T> implements Statable {

    private int numOfReads;
    private int numOfWrites;

    /** Declaring StatableArray to make checkstyle happy.
        @param n the location in the array.
        @param t the element type.
     */
    public StatableArray(int n, T t) {
        super(n, t);
        this.numOfReads = 0;
        this.numOfWrites = 0;
    }

    /** Method to reset statistics.
     */
    public void resetStatistics() {
        this.numOfReads = 0;
        this.numOfWrites = 0;
    }

    /** Method to track number of reads.
        @return number of reads
     */
    public int numberOfReads() {
        return this.numOfReads;
    }

    /** Method to track number of writes.
        @return number of writes
    */
    public int numberOfWrites() {
        return this.numOfWrites;
    }

    @Override
    public T get(int i) {
        this.numOfReads++;
        return super.get(i);
    }

    @Override
    public void put(int i, T t) throws IndexException {
        super.put(i, t);
        this.numOfWrites++;
    }

    @Override
    public int length() {
        this.numOfReads++;
        return super.length();
    }

    /** Main method testing.
        @param args not used.
     */
    public static void main(String[]args) {

        StatableArray<Integer> a = new StatableArray(10, null);
        a.length();
        a.put(7, 5);
        a.get(5);
        System.out.println(a.numberOfReads());
        System.out.println(a.numberOfWrites());
        a.resetStatistics();
        System.out.println(a.numberOfReads());
        System.out.println(a.numberOfWrites());
    }

}
