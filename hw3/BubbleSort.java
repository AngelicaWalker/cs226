/** Sorting algorithm for Bubble Sort.
    @author Angelica Walker
    awalke57
    awalke57@jhu.edu
    600.226.02
    02/25/17
    Assignment 3 Part 2
 */

/** Sorting algorithm for Bubble Sort.
    @param <T> Element Type.
 */
public final class BubbleSort<T extends Comparable<T>>
    implements SortingAlgorithm<T> {


    private boolean less(T a, T b) {
        return a.compareTo(b) < 0;
    }

    private void swap(Array<T> a, int i, int j) {
        T t = a.get(i);
        a.put(i, a.get(j));
        a.put(j, t);
    }

    private boolean sortCheck(Array<T> ra) {
        for (int i = 0; i < ra.length() - 1; i++) {
            if (this.less(ra.get(i + 1), ra.get(i))) {
                return false;
            }
        }
        return true;
    }

    @Override
    public void sort(Array<T> array) {
        while (!this.sortCheck(array)) {
            for (int i = 0; i < array.length() - 1; i++) {
                if (this.less(array.get(i + 1), array.get(i))) {
                    this.swap(array, i, i + 1);
                    if (this.sortCheck(array)) {
                        return;
                    }
                }
            }
        }
    }

    @Override
    public String name() {
        return "Bubble Sort";
    }
}
