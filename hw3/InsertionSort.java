/** Sorting algorithm for Insertion Sort.
    @author Angelica Walker
    awalke57
    awalke57@jhu.edu
    600.226.02
    02/25/17
    Assignment 3 Part 2
 */

/** Sorting algorithm for Insertion Sort.
    @param <T> Element Type.
 */
public final class InsertionSort<T extends Comparable<T>>
    implements SortingAlgorithm<T> {


    private boolean less(T a, T b) {
        return a.compareTo(b) < 0;
    }

    private void swap(Array<T> a, int i, int j) {
        T t = a.get(i);
        a.put(i, a.get(j));
        a.put(j, t);
    }

    @Override
    public void sort(Array<T> ra) {
        for (int i = 1; i < ra.length(); i++) {
            if (this.less(ra.get(i), ra.get(i - 1))) {
                this.swap(ra, i, i - 1);
                for (int j = i - 1; j > 0; j--) {
                    if (this.less(ra.get(j), ra.get(j - 1))) {
                        this.swap(ra, j, j - 1);
                    }
                }
            }
        }
    }

    @Override
    public String name() {
        return "Insertion Sort";
    }
}
