/** Sorting algorithm that doesn't sort.
    @author Angelica Walker
    awalke57
    awalke57@jhu.edu
    600.226.02
    02/25/17
    Assignment 3 Part 2
 */


/**
 * A sorting algorithm that doesn't sort.
 *
 * This is provided mostly as a sanity check: No other algorithm should be
 * faster than this one.
 *
 * @param <T> Element type.
 */
public final class NullSort<T extends Comparable<T>>
    implements SortingAlgorithm<T> {

    @Override
    public void sort(Array<T> array) {
        // nothing to do
    }

    @Override
    public String name() {
        return "Null Sort";
    }
}
